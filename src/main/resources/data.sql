--test data
drop table if exists Inventory;
drop table if exists Movies;

create table Movies (
id int auto_increment PRIMARY KEY,
name varchar(250) not null,
price decimal(20, 2) not null
);

insert into Movies (name, price) values
('kanikuli v manille', 10),
('pulp fiction', 20),
('moto show', 39),
('nothing special', 40.5),
('present continuos', 0),
('nevskii the best', 909.112);

create table Inventory (
    id int auto_increment not null primary key,
    qty int not null,
    movie_id int not null
);

insert into Inventory (movie_id, qty) values
(1, 10),
(2, 5),
(3, 0),
(4, 1),
(5, 2),
(6, 11);

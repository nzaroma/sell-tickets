package ru.roman.selltickets.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.roman.selltickets.model.Movie;

import java.util.List;

@Repository
public interface MoviesDao extends CrudRepository<Movie, Long> {

    List<Movie> findAll();
}

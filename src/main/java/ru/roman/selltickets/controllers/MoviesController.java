package ru.roman.selltickets.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.roman.selltickets.dto.MovieDto;
import ru.roman.selltickets.facades.MovieFacade;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MoviesController {

    @Autowired
    private MovieFacade movieFacade;

    @GetMapping
    public List<MovieDto> getMovies() {
        return movieFacade.getAllMovies();
    }
}

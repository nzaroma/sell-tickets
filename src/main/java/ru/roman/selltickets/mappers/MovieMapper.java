package ru.roman.selltickets.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.roman.selltickets.dto.MovieDto;
import ru.roman.selltickets.model.Movie;

@Mapper
public interface MovieMapper {

    @Mapping(source = "inventory.qty", target = "qty")
    MovieDto map(Movie movie);
}

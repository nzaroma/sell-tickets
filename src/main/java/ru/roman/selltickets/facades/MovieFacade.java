package ru.roman.selltickets.facades;

import ru.roman.selltickets.dto.MovieDto;

import java.util.List;

public interface MovieFacade {

    List<MovieDto> getAllMovies();
}

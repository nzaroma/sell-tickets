package ru.roman.selltickets.facades.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.roman.selltickets.dto.MovieDto;
import ru.roman.selltickets.facades.MovieFacade;
import ru.roman.selltickets.mappers.MovieMapper;
import ru.roman.selltickets.services.MovieService;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieFacadeImpl implements MovieFacade {

    @Autowired
    private MovieService movieService;
    @Autowired
    private MovieMapper movieMapper;

    public List<MovieDto> getAllMovies() {
        return movieService.getAllMovies().stream()
                .map(m -> movieMapper.map(m))
                .collect(Collectors.toList());
    }
}

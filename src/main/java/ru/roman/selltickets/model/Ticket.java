package ru.roman.selltickets.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Ticket {
    private String id;
    private Movie movie;
    private BigDecimal price;
}

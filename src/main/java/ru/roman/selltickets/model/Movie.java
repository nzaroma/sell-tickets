package ru.roman.selltickets.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
//jpa
@Entity
@Table(name = "Movies")
public class Movie {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private BigDecimal price;
    @OneToOne(mappedBy = "movie", cascade = CascadeType.ALL)
    private Inventory inventory;
}

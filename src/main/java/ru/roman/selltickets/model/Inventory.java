package ru.roman.selltickets.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
public class Inventory {
    @Id
    @GeneratedValue
    private Long id;
    private Integer qty;

    @OneToOne
    private Movie movie;
}

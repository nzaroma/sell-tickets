package ru.roman.selltickets.services;

import ru.roman.selltickets.model.Movie;

import java.util.List;

public interface MovieService {

    List<Movie> getAllMovies();
}

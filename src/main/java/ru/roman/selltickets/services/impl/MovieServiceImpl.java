package ru.roman.selltickets.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.roman.selltickets.dao.MoviesDao;
import ru.roman.selltickets.model.Movie;
import ru.roman.selltickets.services.MovieService;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MoviesDao moviesDao;

    public List<Movie> getAllMovies() {
        return moviesDao.findAll();
    }
}

package ru.roman.selltickets.mappers;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import ru.roman.selltickets.dto.MovieDto;
import ru.roman.selltickets.model.Inventory;
import ru.roman.selltickets.model.Movie;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-06-21T20:19:39+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl implements MovieMapper {

    @Override
    public MovieDto map(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDto movieDto = new MovieDto();

        Integer qty = movieInventoryQty( movie );
        if ( qty != null ) {
            movieDto.setQty( qty );
        }
        movieDto.setId( movie.getId() );
        movieDto.setName( movie.getName() );
        movieDto.setPrice( movie.getPrice() );

        return movieDto;
    }

    private Integer movieInventoryQty(Movie movie) {
        if ( movie == null ) {
            return null;
        }
        Inventory inventory = movie.getInventory();
        if ( inventory == null ) {
            return null;
        }
        Integer qty = inventory.getQty();
        if ( qty == null ) {
            return null;
        }
        return qty;
    }
}
